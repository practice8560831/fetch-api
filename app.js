


// Define an empty array to store API data
let dataArray = [];

// Function to fetch data from the API (replace with your actual API endpoint)
async function fetchData() {
    try {
        // Fetch data from the API
        const apiResponse = await fetch("https://jsonplaceholder.typicode.com/todos");
        if (!apiResponse.ok) {
            throw new Error("Network response was not ok");
        }

        const apiData = await apiResponse.json();
        console.log(apiData);

        // Add the API response data to the dataArray
        dataArray = apiData;

        // Call the function to populate the table
        populateTable();
    } catch (error) {
        console.error("Error fetching data:", error);
    }
}

// Function to populate the table with data
function populateTable() {
    const tableBody = document.getElementById("data-table-body");

    // Clear the existing table rows
    tableBody.innerHTML = "";

    // Loop through the dataArray and create table rows
    dataArray.forEach((data) => {
        const row = document.createElement("tr");
        row.innerHTML = `
            <td>${data.userId}</td>
            <td>${data.id}</td>
            <td>${data.title}</td>
            <td><button onclick="fetchObject(${data.id})">Click here</button></td>
        `;
        tableBody.appendChild(row);
    });
}



// Function to fetch an object from the second API

async function fetchObject(id) {
    const loader = document.querySelector(".loader");
    const objectData = document.getElementById("object-data");
    const resultBox = document.getElementById("result-box");

    loader.style.display = "block";
    objectData.style.display = "none";
    resultBox.style.display = "none";

    try {
        // Simulated API response for the second API (replace with your actual API endpoint)
        const apiResponse = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
        if (!apiResponse.ok) {
            throw new Error("Network response was not ok");
        }
        const apiData = await apiResponse.json();
        console.log(apiData);

        // PENDING SETTIME OUT
        objectData.style.display = "none";
        
        setTimeout(() => {
            loader.style.display = "none";
            objectData.style.display = 'block';
           
        }, 2000);
        
        // Display the object data
        objectData.innerHTML = `
            <h2>User ID: ${apiData.userId}</h2>
            <h2>ID: ${apiData.id}</h2>
            <p>Title: ${apiData.title}</p>
        `;
        resultBox.style.display = "block";

    } catch (error) {
        console.error("Error fetching object:", error);
        loader.style.display = "none";
        objectData.innerHTML = "Error fetching object.";
        objectData.style.display = "block";
        resultBox.style.display = "block";
    }
}



// Fetch data from the first API when the page loads
fetchData();